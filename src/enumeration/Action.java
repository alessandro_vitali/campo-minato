package enumeration;

/**
 * enumeration per gestire il tipo di aziona da fare sulla cella
 * 
 * @author Alessandro
 *
 */
public enum Action {
	/**
	 * click sulla cella normale
	 */
	PLAY, 
	/**
	 * click destro per aggiungere la bandiera
	 */
	SET_FLAG, 
	/**
	 * click destro per rimuovere la bandiera
	 */
	REMOVE_FLAG

}
