package interfaces;

import javax.swing.JButton;

/**
 * interfaccia di PanelEasyGame
 * 
 * @author Alessandro
 *
 */
public interface PanelEasyGameInterface {
	
  /**
   * 
   * @return un array di bottoni
   */
	public JButton[][] getButtons();

}
