package interfaces;

/**
 * interfaccia di PanelDifficult, PanelRecord
 * 
 * @author Alessandro
 *
 */
public interface PanelDifficultInterface {
	
  /**
   * aggiorna la view in base al tema
   */
	public void updateThemeView();

}
